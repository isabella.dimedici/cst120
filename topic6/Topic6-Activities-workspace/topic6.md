# Topic 6

- This is my topic 6 assignment.

## HTML code

- [index.html](./6/index.html)
- [tanto.html](./Milestone6/tanto.html)
- [warhammer](./Milestone6/warhammer.html)


## Documentation

-  

![First jQuery Code](firstjquerycode.png)

![Example1b](example1b.png)

![Example1c](example1c.png)


![Sololearn]()

According to the sololearn webpage the jQuery has been deprecated. I was unable to locate it in the catalog.

## Step 7 
### Ipad and phone display

![ipadandiphone](ipadandiphone.png)

## Mac Desktop
![macdesktop](macdesktop.png)

### NavBar
![Desktop](desktop.png)

