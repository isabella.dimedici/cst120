# Topic 5

- This is my topic 5 assignment

## HTML code

- [index.html](./Milestone5/index.html)
- [tanto.html](./Milestone5/tanto.html)


## Documentation

- This is the conditions section. I have created a function named conditions() and added it to my file i named mycode.js. My code shows examples of variable declarations, if statements with numeric comparisons, if statements with string comparisons,switch statements with numeric comparison, ternary operator. 

![Conditions](conditions.png)

- This is the loops section. Shows different loop structures in JavaScript. The loops displayed have the 'for' loops,'while' loops, and the 'do while' loop. These loops show a  many of ways to manage arrays with efficiency.  

![Loops](loops.png)

- This is the objects. in this section displays the way 'objects' can be created and worked with in Javascript. this section creates objects for 'person1' and person2'. This section also adds to the object like the persons characteristics. 


![Objects](objects.png)

![Sololearn](sololearn%20copy.png)