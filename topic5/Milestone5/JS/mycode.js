// Darkmode added
function DarkMode() {
  console.log("dark mode");
  document.body.classList.toggle("dark-mode");
}

// Function to initialize after Html is loaded
function initialize() {
  // Add event listener to the dark mode  button
  document.getElementById("darkMode").addEventListener("click", DarkMode);
}

// Listener to call initialize function when page is fully loaded * forgot to add this wasnt working for a while
window.addEventListener("load", initialize);

// Event handler
function changeWordsToSmile() {
  const textElement = document.getElementById("changeableText");
  textElement.innerHTML = "&#x1F600;";
}
