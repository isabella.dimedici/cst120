# Topic 7

- This is my topic 7 assignment.

## HTML code

- [index.html](./6/index.html)
- [tanto.html](./Milestone6/tanto.html)
- [warhammer](./Milestone6/warhammer.html)


## Documentation

- Example 1a 

![contactnav](contactnav.jpeg)

- Example 1b 

![iphonecontact](iphonecontact.jpeg)

- Example 1c (Contact Form post response)

![contactresponse](contactresponse.jpeg)
