<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Page Title</title>

    <script>
        function playjs() {
            // VARIABLES and ASSIGNMENTS:
            var age = 60;
            let name = 'mark';

            console.log("My name is " + name);
            console.log("My age is " + age);

            // MATH OPERATORS AND COMPARISON OPERATORS
            var number1 = 10;
            var number2 = 5;

            var result = number1 + number2;
            console.log("Addition result is " + result);

            result = number1 - number2;
            console.log("Subtraction result is " + result);

            result = number1 * number2;
            console.log("Multiplication result is " + result);

            result = number1 / number2;
            console.log("Division result is " + result);

            ++number1;
            console.log("Increment result is " + number1);

            --number1;
            console.log("Decrement result is " + number1);

            result = number1 ** number2;
            console.log("Exponentiation result is " + result);

            console.log("Equality for 10 is " + (number1 == 10));
            console.log("Equality for 2 numbers " + (number1 == number2));
            console.log("Equality for 2 numbers and type " + (number1 === number2));
            console.log("Inequality for 2 numbers " + (number1 != number2));
            console.log("Greater for 2 numbers " + (number1 > number2));
            console.log("Less than for 2 numbers " + (number1 < number2));

            // Array
            let shapes1 = ['Triangle', 'Circle', 'Square'];
            let shapes2 = new Array('Triangle', 'Circle', 'Square');

            console.log("First shape is " + shapes1[0]);
            console.log("Last shape is " + shapes1[shapes1.length - 1]);

            // Common Array Methods 
            shapes1.sort();
            console.log("First shape in sorted array is " + shapes1[0]);
            console.log("Last shape in the sorted array is " + shapes1[shapes1.length - 1]);
            shapes1.reverse();
            console.log("First shape in reversed array is " + shapes1[0]);
            console.log("Last shape in reversed array is " + shapes1[shapes1.length - 1]);
            shapes1.push('rectangle');
            console.log("Last shape in array is " + shapes1[shapes1.length - 1]);
            shapes1.pop();
            console.log("Last shape in spliced array is " + shapes1[shapes1.length - 1]);
            shapes1.splice(0, 0, 'Oval');
            console.log("First shape in spliced array is " + shapes1[0]);

            // Iteration
            for (let shape of shapes1) {
                console.log("The shape is " + shape);
            }

            // DATES AND FUNCTIONS
            let now = new Date();
            let birthday = new Date(1960, 9, 17, 0, 0, 0);

            console.log("Current date and time is " + now);
            console.log("Past date and time is " + birthday);
            console.log("Time difference is " + (now.getFullYear() - birthday.getFullYear()));

            // FUNCTIONS 
            reusableCode();
            result = add(15, 20);
            console.log("Result is " + result);
            result = multiply(15, 20);
            console.log("Result is " + result);
            shapes1.forEach(displayArray);

            function reusableCode() {
                console.log("Called the reusableCode() function");
            }

            function add(num1, num2) {
                console.log("Adding numbers " + num1 + " and " + num2);
                return num1 + num2;
            }

            function multiply(num1, num2) {
                console.log("Multiplying numbers " + num1 + " and " + num2);
                return num1 * num2; // Added return statement
            }

            function displayArray(value, index) {
                console.log("From displayArray() the array value is " + value + " at " + index);
            }
        }
    </script>
</head>

<body onload="playjs()">
    <p></p>
</body>

</html>
